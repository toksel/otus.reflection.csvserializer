﻿namespace otus.reflection.csvserializer
{
   public static class Extension
   {
      public static string Serialize<T>(this T obj)
         => new CsvSerializer<T>(obj).Serialize();
      public static string Serialize<T>(this T obj, char delimiter)
         => new CsvSerializer<T>(obj, delimiter).Serialize();
      public static string Serialize<T>(this T obj, bool includeHeader)
         => new CsvSerializer<T>(obj, includeHeader).Serialize();
      public static string Serialize<T>(this T obj, char delimiter, bool includeHeader)
         => new CsvSerializer<T>(obj, delimiter, includeHeader).Serialize();
      public static T Deserealize<T>(this string scv)
         => new CsvSerializer<T>().Deserialize(scv);
      public static T Deserealize<T>(this string scv, char delimiter)
         => new CsvSerializer<T>(delimiter).Deserialize(scv);
      public static T Deserealize<T>(this string scv, bool hasHeader)
         => new CsvSerializer<T>(hasHeader).Deserialize(scv);
      public static T Deserealize<T>(this string scv, char delimiter, bool hasHeader)
         => new CsvSerializer<T>(delimiter, hasHeader).Deserialize(scv);
   }
}
