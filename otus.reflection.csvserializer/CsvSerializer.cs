﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace otus.reflection.csvserializer
{
   class CsvSerializer<T>
   {
      T _obj;
      char _delimiter = ',';
      bool _hasHeader = false;

      public CsvSerializer() { }
      public CsvSerializer(char delimiter)
      {
         _delimiter = delimiter;
      }

      public CsvSerializer(bool includeHeader)
      {
         _hasHeader = includeHeader;
      }

      public CsvSerializer(char delimiter, bool hasHeader) : this(delimiter)
      {
         _hasHeader = hasHeader;
      }

      public CsvSerializer (T item, char delimiter = ',')
      {
         _obj = item;
         _delimiter = delimiter;
      }

      public CsvSerializer(T item, bool includeHeader) : this(item)
      {
         _hasHeader = includeHeader;
      }

      public CsvSerializer(T item, char delimiter, bool includeHeader) : this(item, delimiter)
      {
         _hasHeader = includeHeader;
      }

      public string Serialize()
      {
         Type type = _obj.GetType();
         FieldInfo[] fields = type.GetFields(BindingFlags.Instance |
                                             BindingFlags.NonPublic |
                                             BindingFlags.Public);
         StringBuilder sb = new StringBuilder();
         if (_hasHeader)
         {
            sb.AppendLine(fields.Select(s => s.Name).Aggregate((a1, a2) => a1 + _delimiter + a2));
         }
         sb.Append(fields.Select(s => s.GetValue(_obj).ToString()).Aggregate((a1, a2) => a1 + _delimiter + a2));
         return sb.ToString();
      }

      public T Deserialize(string csv)
      {
         string[] parameters = _hasHeader ? 
                               csv.Split(Environment.NewLine)[1].Split(_delimiter) :
                               csv.Split(_delimiter);
         Type type = typeof(T);
         object obj = Activator.CreateInstance(type);
         FieldInfo[] fields = type.GetFields(BindingFlags.Instance |
                                             BindingFlags.NonPublic |
                                             BindingFlags.Public);
         if (fields.Length != parameters.Length)
         {
            throw new ArgumentException($"Faild to deserialize input string to object with type {type.Name}");
         }
         for (int i = 0; i < fields.Length; i++)
         {
            fields[i].SetValue(obj, Convert.ChangeType(parameters[i], fields[i].FieldType));
         }
         return (T)obj;
      }
   }
}
