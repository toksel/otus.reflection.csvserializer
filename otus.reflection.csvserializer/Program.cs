﻿using Newtonsoft.Json;
using System;

namespace otus.reflection.csvserializer
{
   class Program
   {
      static void Main(string[] args)
      {
         F testF = new F().Get();
         string csv = "11;22;33;44;55";
         string json = "{\"i1\":11,\"i2\":22,\"i3\":33,\"i4\":44,\"i5\":55}";

         int attempts = 10000;

         Console.WriteLine("Serialization result:");
         Console.WriteLine(testF.Serialize(true));
         Console.WriteLine("Deserialization result:");
         Console.WriteLine(csv.Deserealize<F>(';'));

         #region(Serialization benchmark)

         Console.WriteLine("Serialization benchmark start (csv):");
         DateTime startTime = DateTime.Now;
         Console.WriteLine($"start: {startTime}");
         for (int i = 0; i < attempts; i++)
         {
            testF.Serialize();
         }
         DateTime endTime = DateTime.Now;
         double duration = (endTime - startTime).TotalMilliseconds;
         Console.WriteLine($"start: {startTime}");
         Console.WriteLine($"duration: {duration} ms");
         Console.WriteLine("Serialization benchmark end (csv)\n");


         Console.WriteLine("Serialization benchmark start (json):");
         startTime = DateTime.Now;
         Console.WriteLine($"start: {startTime}");
         for (int i = 0; i < attempts; i++)
         {
            JsonConvert.SerializeObject(testF);
         }
         endTime = DateTime.Now;
         duration = (endTime - startTime).TotalMilliseconds;
         Console.WriteLine($"start: {startTime}");
         Console.WriteLine($"duration: {duration} ms");
         Console.WriteLine("Serialization benchmark end (json)\n");

         #endregion

         #region(Deserialization benchmark)

         Console.WriteLine("Deserialization benchmark start (csv):");
         startTime = DateTime.Now;
         Console.WriteLine($"start: {startTime}");
         for (int i = 0; i < attempts; i++)
         {
            csv.Deserealize<F>(';');
         }
         endTime = DateTime.Now;
         duration = (endTime - startTime).TotalMilliseconds;
         Console.WriteLine($"start: {startTime}");
         Console.WriteLine($"duration: {duration} ms");
         Console.WriteLine("Deserialization benchmark end (csv)\n");


         Console.WriteLine("Deserialization benchmark start (json):");
         startTime = DateTime.Now;
         Console.WriteLine($"start: {startTime}");
         for (int i = 0; i < attempts; i++)
         {
            JsonConvert.DeserializeObject<F>(json);
         }
         endTime = DateTime.Now;
         duration = (endTime - startTime).TotalMilliseconds;
         Console.WriteLine($"start: {startTime}");
         Console.WriteLine($"duration: {duration} ms");
         Console.WriteLine("Deserialization benchmark end (json)\n");

         #endregion

         Console.ReadKey();
      }
   }
}
